# Pour builder le container: docker build -t my-apache .
# Pour lancer le container: docker run --rm -d -p 1337:80 --name my-apache my-apache
# Pour entrer dans le container: docker exec -it my-apache bash
# docker exec -e COLUMNS=120 -it my-apache bash

FROM httpd:2.4
RUN echo "deb https://packages.sury.org/php/ stretch main" | tee /etc/apt/sources.list.d/php.list
RUN apt update
RUN apt upgrade -y
RUN apt install ca-certificates apt-transport-https wget -y
RUN wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
RUN apt update
RUN apt install php7.2 -y
COPY ./public-html/ /usr/local/apache2/htdocs/
